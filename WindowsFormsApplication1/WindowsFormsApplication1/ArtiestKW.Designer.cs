﻿namespace WindowsFormsApplication1
{
    partial class ArtiestKW
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKunstwerk = new System.Windows.Forms.Label();
            this.lblJaar = new System.Windows.Forms.Label();
            this.lblLink = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // lblKunstwerk
            // 
            this.lblKunstwerk.AutoSize = true;
            this.lblKunstwerk.Location = new System.Drawing.Point(3, 0);
            this.lblKunstwerk.Name = "lblKunstwerk";
            this.lblKunstwerk.Size = new System.Drawing.Size(71, 17);
            this.lblKunstwerk.TabIndex = 0;
            this.lblKunstwerk.Text = "kunstwerk";
            // 
            // lblJaar
            // 
            this.lblJaar.AutoSize = true;
            this.lblJaar.Location = new System.Drawing.Point(164, 0);
            this.lblJaar.Name = "lblJaar";
            this.lblJaar.Size = new System.Drawing.Size(32, 17);
            this.lblJaar.TabIndex = 1;
            this.lblJaar.Text = "jaar";
            // 
            // lblLink
            // 
            this.lblLink.AutoSize = true;
            this.lblLink.Location = new System.Drawing.Point(258, 0);
            this.lblLink.Name = "lblLink";
            this.lblLink.Size = new System.Drawing.Size(70, 17);
            this.lblLink.TabIndex = 2;
            this.lblLink.TabStop = true;
            this.lblLink.Text = "lees meer";
            // 
            // ArtiestKW
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblLink);
            this.Controls.Add(this.lblJaar);
            this.Controls.Add(this.lblKunstwerk);
            this.Name = "ArtiestKW";
            this.Size = new System.Drawing.Size(386, 20);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKunstwerk;
        private System.Windows.Forms.Label lblJaar;
        private System.Windows.Forms.LinkLabel lblLink;
    }
}
