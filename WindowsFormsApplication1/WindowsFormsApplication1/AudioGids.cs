﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class AudioGids : UserControl, Observer
    {
        private Archief model;
        private Kunstwerk kwModel;
        private Artiest artiestModel;
        Kunstwerk[] kunstwerken;

        public AudioGids()
        {
            InitializeComponent();
        }
        public void setModel(Archief model)
        {
            this.model = model;
            model.addObserver(this);
        }

        private void btnId_Click(object sender, EventArgs e)
        {
            Controller.Instance.idClicked(Convert.ToInt32(txtId.Text));
            //kwModel = model.getKunstwerk(Convert.ToInt32(txtId.Text));
            kunstwerkUI1.Visible = true;
            artiestUI1.Visible = false;
            ModelChanged();
        }
        public void ModelChanged()
        {
            kunstwerkUI1.ModelChanged();
            artiestUI1.ModelChanged();
            //kwModel = model.getKunstwerk(Convert.ToInt32(txtId.Text));
            //kunstwerkUI1.setModel(kwModel);
            


        }

        private void btnArtiest_Click(object sender, EventArgs e)
        {
            Controller.Instance.artiestClicked(txtArtiest.Text);
            artiestUI1.ModelChanged();
            artiestUI1.Visible = true;
            kunstwerkUI1.Visible = false;
            //artiestModel = model.getArtiest(txtArtiest.Text);
            //kunstwerken = new Kunstwerk[10];
            //kunstwerken = model.getKunstwerken(artiestModel);
            
            
            Refresh();
        }
        
    }
}
