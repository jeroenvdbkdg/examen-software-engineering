﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ArtiestKW : UserControl
    {
        public ArtiestKW(Kunstwerk k)
        {
            InitializeComponent();
            lblKunstwerk.Text = k.getNaam();
            lblJaar.Text = Convert.ToString(k.Jaar);
            lblLink.Text = Convert.ToString(k.Nummer);

        }
        public ArtiestKW()
        {
            InitializeComponent();
        }
    }
}
