﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ArtiestUI : UserControl
    {
        private ArtiestKW[] kunstwerken;

        public ArtiestUI()
        {
            InitializeComponent();
            kunstwerken = new ArtiestKW[10];
        }
        
        
        public void ModelChanged()
        {
            foreach (ArtiestKW k in kunstwerken)
            {
                Controls.Remove(k);
            }
            int i = 0;
            if (Controller.Instance.Kunstwerken != null)
            {
                if(Controller.Instance.Kunstwerken[0] != null)
                    lblArtiest.Text = Controller.Instance.Kunstwerken[0].getArtiest().Naam;
                foreach (Kunstwerk k in Controller.Instance.Kunstwerken)
                {
                    if (k != null)
                    {
                        kunstwerken[i] = new ArtiestKW(k);
                        kunstwerken[i].Location = new Point(0, 52 + i * 20);
                        Controls.Add(kunstwerken[i]);
                        i++;
                    }
                    
                }
            }
        }
        
        public ArtiestKW[] Artiestkw
        {
            set { value = kunstwerken; }
        }
    }
}
