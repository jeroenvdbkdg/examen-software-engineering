﻿namespace WindowsFormsApplication1
{
    partial class AudioGids
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnArtiest = new System.Windows.Forms.Button();
            this.btnId = new System.Windows.Forms.Button();
            this.txtArtiest = new System.Windows.Forms.TextBox();
            this.txtId = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.kunstwerkUI1 = new WindowsFormsApplication1.KunstwerkUI();
            this.artiestUI1 = new WindowsFormsApplication1.ArtiestUI();
            this.SuspendLayout();
            // 
            // btnArtiest
            // 
            this.btnArtiest.Location = new System.Drawing.Point(324, 69);
            this.btnArtiest.Name = "btnArtiest";
            this.btnArtiest.Size = new System.Drawing.Size(75, 23);
            this.btnArtiest.TabIndex = 14;
            this.btnArtiest.Text = "zoeken";
            this.btnArtiest.UseVisualStyleBackColor = true;
            this.btnArtiest.Click += new System.EventHandler(this.btnArtiest_Click);
            // 
            // btnId
            // 
            this.btnId.Location = new System.Drawing.Point(324, 40);
            this.btnId.Name = "btnId";
            this.btnId.Size = new System.Drawing.Size(75, 23);
            this.btnId.TabIndex = 13;
            this.btnId.Text = "zoeken";
            this.btnId.UseVisualStyleBackColor = true;
            this.btnId.Click += new System.EventHandler(this.btnId_Click);
            // 
            // txtArtiest
            // 
            this.txtArtiest.Location = new System.Drawing.Point(129, 69);
            this.txtArtiest.Name = "txtArtiest";
            this.txtArtiest.Size = new System.Drawing.Size(168, 22);
            this.txtArtiest.TabIndex = 12;
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(129, 40);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(168, 22);
            this.txtId.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(31, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 10;
            this.label3.Text = "artiest";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(134, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 32);
            this.label1.TabIndex = 8;
            this.label1.Text = "audiogids";
            // 
            // kunstwerkUI1
            // 
            this.kunstwerkUI1.Location = new System.Drawing.Point(31, 99);
            this.kunstwerkUI1.Name = "kunstwerkUI1";
            this.kunstwerkUI1.Size = new System.Drawing.Size(386, 287);
            this.kunstwerkUI1.TabIndex = 0;
            this.kunstwerkUI1.Visible = false;
            // 
            // artiestUI1
            // 
            this.artiestUI1.Location = new System.Drawing.Point(15, 107);
            this.artiestUI1.Name = "artiestUI1";
            this.artiestUI1.Size = new System.Drawing.Size(386, 287);
            this.artiestUI1.TabIndex = 15;
            this.artiestUI1.Visible = false;
            // 
            // AudioGids
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.artiestUI1);
            this.Controls.Add(this.btnArtiest);
            this.Controls.Add(this.btnId);
            this.Controls.Add(this.txtArtiest);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.kunstwerkUI1);
            this.Name = "AudioGids";
            this.Size = new System.Drawing.Size(442, 398);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private KunstwerkUI kunstwerkUI1;
        private System.Windows.Forms.Button btnArtiest;
        private System.Windows.Forms.Button btnId;
        private System.Windows.Forms.TextBox txtArtiest;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ArtiestUI artiestUI1;
    }
}
