﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Archief: Observable
    {
        private List<Artiest> artiesten = new List<Artiest>();
        private List<Kunstwerk> kunstwerken = new List<Kunstwerk>();

        private Artiest gekozenArtiest;
        private Kunstwerk gekozenKunstwerk;
        private Kunstwerk[] gekozenKunstwerken;
        
        private Artiest pollock;
        private Artiest notFound;
        private Artiest picasso;
        private Artiest magritte;
        private Artiest delacroix;

        private int artiestId;
        private int kunstwerkId;

        private bool showKunstwerk;

        public Archief()
        {
            artiestId = 0;
            kunstwerkId = 0;
            addArtiesten();
            addKunstwerken();
        }
        public bool ShowKunstwerk
        {
            get { return showKunstwerk; }
            set { showKunstwerk = value; }
        }

        public Artiest Artiest
        {
            get { return gekozenArtiest; }
            
        }
        public Kunstwerk Kunstwerk
        {
            get { return gekozenKunstwerk; }
           
        }
        public Kunstwerk[] Kunstwerken
        {
            get { return gekozenKunstwerken; }
        }


        /*public void addView(AudioGids view)
        {
            views.Add(view);
            NotifyObservers();
        }*/
        /*private void UpdateViews()
        {
            foreach (AudioGids view in views)
                view.Update();
        }*/
        public void getKunstwerk(int nummer)
        {
            gekozenKunstwerken = new Kunstwerk[10];
            foreach (Kunstwerk k in kunstwerken)
            {
                if (k.Nummer == nummer)
                {
                    gekozenKunstwerk = k;
                    return;
                } 
                
            }
             gekozenKunstwerk = kunstwerken[0];
        }
        public void getArtiest(string artiestNaam)
        {
            foreach (Artiest a in artiesten)
            {
                if (a.Naam.Contains(artiestNaam))
                {
                    gekozenArtiest = a;
                    getKunstwerken();
                    return;
                }
            }
            gekozenArtiest = notFound;
        }
        public void getKunstwerken()
        {
            //Kunstwerk[] artiestWerk = new Kunstwerk[kunstwerken.Count()];
            gekozenKunstwerken = new Kunstwerk[10];

            int count = 0;
            foreach (Kunstwerk k in kunstwerken)
            {
                if (k.getArtiest() == gekozenArtiest)
                {
                    gekozenKunstwerken[count] = k;
                    count++;
                }
            }
            if (gekozenKunstwerken[0] == null)
                gekozenKunstwerken[0] = kunstwerken[0];
            
        }
        /*public Artiest[] getArtiesten()
        {
            return artiesten;
        }*/
        private void addArtiesten()
        {
            notFound = new Artiest("not found", 0, artiestId++);
            artiesten.Add(notFound);
            pollock = new Artiest("Jackson Pollock", 1912, artiestId++);
            artiesten.Add(pollock);
            picasso = new Artiest("Pablo Picasso", 1881, artiestId++);
            artiesten.Add(picasso);
            magritte = new Artiest("René Magritte", 1898, artiestId++);
            artiesten.Add(magritte);
            delacroix = new Artiest("Eugène Delacroix", 1798, artiestId++);
            artiesten.Add(delacroix);
        }
        private void addKunstwerken(){
            Kunstwerk leeg = new Kunstwerk(0, "not found", notFound, 0, "", "");
            kunstwerken.Add(leeg);
            Kunstwerk maleAndFemale = new Kunstwerk(1, 
                "male and female", pollock, 1942, 
                "Male and Female portrays a man and woman using bold colors and in an extreme abstract form. The figure on the right appears to have a blackboard type surface as a body displaying numbers and mathematical symbols. The image to the left is less identifiable except for the appearance of two long lashed eye openings. They appear to be joined in the center by a surface containing 3 triangles and what looks like a partial, almost ghostlike child figure",
                "img/male-and-female.jpg");
            kunstwerken.Add(maleAndFemale);
            Kunstwerk theSheWolf = new Kunstwerk(2,
                "the she wolf", pollock, 1943,
                "When Pollock painted The She-Wolf he had not yet arrived at his so-called 'drip' style, one of the great inventions of Abstract Expressionism. The canvas's traces of multicolored washes and spatters show that a free-form abstraction and an unfettered play of materials were already parts of his process; but in this work and others his focus is a compound of mythology and an iconography of the unconscious. (He was influenced here both by Surrealism and his own Jungian analysis.)",
                "img/the-She-Wolf.jpg");
            kunstwerken.Add(theSheWolf);
            Kunstwerk guernica = new Kunstwerk(3,
                "guernica", picasso, 1937,
                "Probably Picasso's most famous work, Guernica is certainly the his most powerful political statement, painted as an immediate reaction to the Nazi's devastating casual bombing practice on the Basque town of Guernica during Spanish Civil War.",
                "img/guernica.jpg");
            kunstwerken.Add(guernica);
        }
    }
}
