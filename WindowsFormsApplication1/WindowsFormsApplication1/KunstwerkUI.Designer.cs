﻿namespace WindowsFormsApplication1
{
    partial class KunstwerkUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNaam = new System.Windows.Forms.Label();
            this.lblArtiest = new System.Windows.Forms.LinkLabel();
            this.lbljaar = new System.Windows.Forms.Label();
            this.lblUitleg = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNaam
            // 
            this.lblNaam.AutoSize = true;
            this.lblNaam.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaam.Location = new System.Drawing.Point(133, 0);
            this.lblNaam.Name = "lblNaam";
            this.lblNaam.Size = new System.Drawing.Size(90, 32);
            this.lblNaam.TabIndex = 0;
            this.lblNaam.Text = "naam";
            this.lblNaam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblArtiest
            // 
            this.lblArtiest.AutoSize = true;
            this.lblArtiest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArtiest.LinkColor = System.Drawing.Color.Black;
            this.lblArtiest.Location = new System.Drawing.Point(178, 41);
            this.lblArtiest.Name = "lblArtiest";
            this.lblArtiest.Size = new System.Drawing.Size(71, 25);
            this.lblArtiest.TabIndex = 1;
            this.lblArtiest.TabStop = true;
            this.lblArtiest.Text = "artiest";
            // 
            // lbljaar
            // 
            this.lbljaar.AutoSize = true;
            this.lbljaar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbljaar.Location = new System.Drawing.Point(188, 70);
            this.lbljaar.Name = "lbljaar";
            this.lbljaar.Size = new System.Drawing.Size(37, 20);
            this.lbljaar.TabIndex = 2;
            this.lbljaar.Text = "jaar";
            // 
            // lblUitleg
            // 
            this.lblUitleg.Location = new System.Drawing.Point(180, 102);
            this.lblUitleg.Name = "lblUitleg";
            this.lblUitleg.Size = new System.Drawing.Size(192, 174);
            this.lblUitleg.TabIndex = 3;
            this.lblUitleg.Text = "uitleg";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(4, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 243);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // KunstwerkUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblUitleg);
            this.Controls.Add(this.lbljaar);
            this.Controls.Add(this.lblArtiest);
            this.Controls.Add(this.lblNaam);
            this.Name = "KunstwerkUI";
            this.Size = new System.Drawing.Size(386, 287);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNaam;
        private System.Windows.Forms.LinkLabel lblArtiest;
        private System.Windows.Forms.Label lbljaar;
        private System.Windows.Forms.Label lblUitleg;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
