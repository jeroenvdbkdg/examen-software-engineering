﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Controller
    {
        private static Controller instance = new Controller();
        private Archief model;
        //private Kunstwerk kwModel;
        //private Artiest aModel;
        //private Kunstwerk[] kunstwerken;


        public static Controller Instance
        {
            get { return instance; }
        }

        private Controller()
        {
            model = new Archief();
            
        }

        public void idClicked(int id)
        {
            model.getKunstwerk(id);
            
            model.NotifyObservers();
        }
        public void artiestClicked(string artiest)
        {
            model.getArtiest(artiest);
            model.NotifyObservers();
        }
        
        public Kunstwerk Kunstwerk
        {
            get { return model.Kunstwerk; }
        }
        public Kunstwerk[] Kunstwerken
        {
            get { return model.Kunstwerken; }
        }
        public Artiest Artiest
        {
            get { return model.Artiest; }
        }
        
    }
}
