﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Kunstwerk: Observable
    {
        private string imageURL;
        private int id;
        private string naam;
        private Artiest artiest;
        private int jaartal;
        private string uitleg;
        

        public Kunstwerk(int id, string naam, Artiest artiest, int jaartal, string uitleg, string imageURL)
        {
            this.id = id;
            this.naam = naam;
            this.artiest = artiest;
            this.jaartal = jaartal;
            this.uitleg = uitleg;
            this.imageURL = imageURL;
        }

        public int Nummer
        {
            get { return id; }
        }

        public string getUitleg()
        {
            return uitleg;
        }

        public Artiest getArtiest()
        {
            return artiest;
        }

        public string getNaam()
        {
            return naam;
        }

        public string getImage()
        {
            return imageURL;
        }

        public int Jaar
        {
            get { return jaartal; }
        }
    }
}
