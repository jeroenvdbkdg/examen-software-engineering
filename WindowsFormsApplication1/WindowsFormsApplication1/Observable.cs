﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Observable
    {
        private List<Observer> observers;

        public Observable()
        {
            observers = new List<Observer>();
        }

        public void addObserver(Observer observer)
        {
            observers.Add(observer);
            observer.ModelChanged();
        }

        public void NotifyObservers()
        {
            foreach (Observer observer in observers)
                observer.ModelChanged();
        }
    }
}
