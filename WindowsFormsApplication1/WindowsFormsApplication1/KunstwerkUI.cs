﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class KunstwerkUI : UserControl, Observer
    {
        Kunstwerk model;

        public void setModel(Kunstwerk model)
        {
            this.model = model;
            ModelChanged();
        }

        public KunstwerkUI()
        {
            InitializeComponent();
        }

        public void ModelChanged()
        {
            if (Controller.Instance.Kunstwerk != null)
            {
                lblArtiest.Text = Controller.Instance.Kunstwerk.getArtiest().Naam;
                lbljaar.Text = Convert.ToString(Controller.Instance.Kunstwerk.Jaar);
                lblNaam.Text = Controller.Instance.Kunstwerk.getNaam();
                lblUitleg.Text = Controller.Instance.Kunstwerk.getUitleg();

                pictureBox1.Invalidate();
            }
        }

        /*private void updateView()
        {
            lblNaam.Text = Controller.Instance.Kunstwerk.getNaam();
            lblUitleg.Text = Controller.Instance.Kunstwerk.getUitleg();
            lbljaar.Text = Convert.ToString(Controller.Instance.Kunstwerk.Jaar);
            lblArtiest.Text = Controller.Instance.Kunstwerk.getArtiest().getNaam();
            Refresh();
        }*/

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics paper = e.Graphics;
            if (Controller.Instance.Kunstwerk != null  )
            {
                Image img = Image.FromFile(Controller.Instance.Kunstwerk.getImage());
                paper.DrawImage(img, 0, 0, pictureBox1.Width, pictureBox1.Height);
            }
        }
    }
}
