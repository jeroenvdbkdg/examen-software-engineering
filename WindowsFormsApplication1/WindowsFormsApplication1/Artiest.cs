﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public class Artiest: Observable
    {
        private string naam;
        private int geboortejaar;
        private int id;

        public Artiest(string naam, int geboortejaar, int id)
        {
            this.naam = naam;
            this.geboortejaar = geboortejaar;
            this.id = id;
        }

        public string getNaam()
        {
            return naam;
        }

        public string Naam
        {
            get { return naam; }
        }
        public int Geboortejaar
        {
            get { return geboortejaar; }
        }
    }
}
